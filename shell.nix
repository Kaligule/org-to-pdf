with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "node";
  buildInputs = [
    emacs # for exporting org to latex
    gnumake # make
    (texlive.combine {
      inherit (texlive)
        scheme-basic
        latexmk wrapfig
        ulem
        capt-of
        collection-fontsrecommended;
    })
  ];
  shellHook = ''
        echo "We now are in a shell that should be able to create documents via make."
    '';
}
